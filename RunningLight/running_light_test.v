`timescale 1us/1us  // Задайте временную шкалу

module running_light_test;

  reg sys_clk;          // Симулируйте тактовый сигнал
  reg sys_rst_n;        // Симулируйте сигнал сброса
  wire [5:0] led;       // Симулируйте выходные LED
  // wire [23:0] counter;  // Симулируйте счетчик

  // Подключите ваш модуль led
  led led_inst (
    .sys_clk(sys_clk),
    .sys_rst_n(sys_rst_n),
    .led(led)
  );

  // Генерация тактового сигнала
  always begin
    #5 sys_clk <= ~sys_clk;
  end

  // Инициализация сигнала сброса и симуляция
  initial begin
    sys_clk <= 0;
    sys_rst_n <= 1;

    // Сброс
    sys_rst_n <= 0;
    #10;
    sys_rst_n <= 1;

    // Симуляция
    #1350000000;  // Запустите симуляцию на 1350000000 тактов

    // Завершение симуляции
    $finish;
  end

  // Создание VCD файла и указание момента начала записи
  initial begin
    $dumpfile("led_simulation.vcd");
    $dumpvars(0, led); // собирает записи только об одном регистре
    // $dumpvars(0, led_inst); // генерит дохера данных led_inst - экземпляр модуля led
  end

endmodule