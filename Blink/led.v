module led (
    input sys_clk,      // clk input
    output reg led = 0,     // 1 LED pin
    output reg led2 = 0
);

reg [25:0] counter = 26'd0; // Счетчик для задержки

always @(posedge sys_clk) begin
    if (counter < 26'd49999999)  // Примерно 2 секунды при 25 МГц - 50000000. Для теста поставлено 5.
        counter <= counter + 1'b1;
    else begin  // Это переключение занимает 1 такт. Поэтому сброс counter нужно делать на 1 такт раньше.
        counter <= 0;
        led <= ~led;  // Инвертируем состояние led
    end
end

endmodule
