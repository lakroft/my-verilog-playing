module top (
    input wire clk,
    input btn1,
    input btn2,
    output reg [5:0] led
);
reg btn_inc_prev, btn_dec_prev; // Предыдущее состояние кнопок

// TODO check ```always @(posedge btn1) begin..```
always @(posedge clk) begin
  // Увеличение счетчика при нажатии кнопки увеличения
  if (btn1 == 1 && btn_inc_prev == 0) begin
    led <= led + 1;
  end

  // Уменьшение счетчика при нажатии кнопки уменьшения
  if (btn2 == 1 && btn_dec_prev == 0) begin
    led <= led - 1;
  end

  // Сохранение текущего состояния кнопок для следующего такта
  btn_inc_prev <= btn1;
  btn_dec_prev <= btn2;
end
endmodule
