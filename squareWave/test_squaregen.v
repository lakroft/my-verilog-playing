`timescale 1us/1us  // Задайте временную шкалу
// ^ - 1-е число - сколько времени занимает 1 такт, 2-е число - как часто мы записываем значения
// они могут быть равны, или второе число может быть меньше(но не больше).

module test_squaregen;

    reg sys_clk;
    reg doPlay;
    reg [20:0] delay;
    wire out;
    
    // Создаем файл для записи волновых форм
    initial begin
        $dumpfile("test_squaregen.vcd");
        $dumpvars(0, out, doPlay);
        $dumpvars(0, uut);
    end
    
    // Подключаем модуль squaregen
    squaregen uut (
        .sys_clk(sys_clk),
        .doPlay(doPlay),
        .delay(delay),
        .out(out)
    );
    
    // Создаем генератор тактового сигнала
    always begin
        #5 sys_clk = ~sys_clk; // Генерируем тактовый сигнал с периодом 10 временных единиц (по 5 тактов вверх и вниз)
    end

    // Создаем тестовые сценарии
    initial begin
        // Начальные значения
        sys_clk = 0;
        doPlay = 0;
        delay = 0;
        
        #100
        
        // Пуск генерации
        doPlay = 1;
        delay = 10; // Устанавливаем задержку
        
        // Ожидаем, что out начнет меняться после задержки
        #1005 $display("out=%b", out);
        
        // Переключаем doPlay, чтобы остановить генерацию
        doPlay = 0;
        #100 $display("out=%b", out);
        
        // Завершаем симуляцию
        $finish;
    end

endmodule
