module squaregen (
    input sys_clk,      // clk input
    input wire doPlay,  // 
    input wire [20:0] delay,
    output reg out = 0     // 1 output pin
);

reg [20:0] counter = 21'd0; // Счетчик для задержки. Для поддержки мин. частоты 20Гц максимальная задержка должна быть 1349999
// макс. частотой можно взять 15кГц

always @(posedge sys_clk) begin
    if (doPlay)
        if (counter < delay)
            counter <= counter + 1'b1;
        else begin  // Это переключение занимает 1 такт. Поэтому сброс counter нужно делать на 1 такт раньше.
            counter <= 0;
            out <= ~out;  // Инвертируем состояние out
        end
end

always @(posedge doPlay) begin
    counter <= 0;
end

always @(negedge doPlay) begin
    out <= 0;
end

endmodule
