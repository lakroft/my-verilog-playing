module led_test;

    reg sys_clk;
    wire led;

    // Подключение вашего модуля "led"
    led led_inst (
        .sys_clk(sys_clk),
        .led(led)
    );

    // Создание генератора тактового сигнала
    always begin
        #10 sys_clk = ~sys_clk;  // Инвертируем тактовый сигнал каждые 10 единиц времени
    end

    reg [2:0] switches = 3'b0;
    reg prev_led = 1'b0;

    // Отладочный вывод для "led"
    always @(posedge sys_clk) begin
        if (led != prev_led) begin
            switches = switches + 1'b1; // do not use '=' inside 'always' in real code, only in tests. Use '<=' instead. 
                                        // with '<=' update will take effect only in next tick but it guarantee stadility. 
                                        // '=' could be used for initial assigning outside of 'always'
            prev_led <= led; // Используйте "<=" для обновления регистра
            $display("LED: %b, Switches: %b, Time: %t", led, switches, $time);
        end
    end


    // Инициализация
    initial begin
        sys_clk = 0;
        prev_led = ~led;

        // Проверяем работу модуля
        #2100000000;  // Дадим модулю работать на протяжении нескольких периодов тактового сигнала 400+ 

        // Ожидаем, что "led" переключится два раза (каждые 2 секунды)
        if (switches === 2'd3)
            $display("Test Passed: LED toggled twice."); // Initial switchind on startup is counted in switches, but I take only worktime switches which is n-1.
        else
            $display("Test Failed: LED did not toggle as expected. Switches: %b, led: %b", switches, led);

        // Завершение симуляции
        $finish;
    end

endmodule
