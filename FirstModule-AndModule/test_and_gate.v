`timescale 1us/1us
// ^ - 1-е число - сколько времени занимает 1 такт, 2-е число - как часто мы записываем значения
// они могут быть равны, или второе число может быть меньше(но не больше).
module test_and_gate;
    reg a, b;
    wire out;

    and_gate uut (
        .a(a),
        .b(b),
        .out(out)
    );

    initial begin
        $dumpfile("simulationWaves.vcd");
        $dumpvars(0, a, b, out);
        // Первый тестовый случай
        a = 0;
        b = 0;
        #5; // Задержка перед отображением результата
        $display("a = %b, b = %b, out = %b", a, b, out);

        // Второй тестовый случай
        a = 1;
        #5;
        b = 0;
        #5;
        $display("a = %b, b = %b, out = %b", a, b, out);

        // Третий тестовый случай
        a = 0;
        #5;
        b = 1;
        #5;
        $display("a = %b, b = %b, out = %b", a, b, out);

        // Четвертый тестовый случай
        a = 1;
        #5;
        b = 1;
        #5;
        $display("a = %b, b = %b, out = %b", a, b, out);

        // Завершение симуляции
        $finish;
    end
endmodule
