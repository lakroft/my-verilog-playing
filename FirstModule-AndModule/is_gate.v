module example_module (
    input some_signal,
    output wire a
);

assign a = some_signal;

endmodule

module test_example_module;

    reg some_signal;
    wire a;

    example_module uut (
        .some_signal(some_signal),
        .a(a)
    );

    initial begin
        // Задаем входной сигнал
        some_signal = 1;

        // Делаем паузу для того, чтобы присвоение произошло
        #5;

        // Выводим значения
        $display("some_signal = %b, a = %b", some_signal, a);

        some_signal = 0;
        #5;
        $display("some_signal = %b, a = %b", some_signal, a);

        // Завершаем симуляцию
        $finish;
    end

endmodule