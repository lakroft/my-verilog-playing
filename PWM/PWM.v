// How to use this module:
    // module my_design (input [31:0] clock);
    // wire [7:0] pwm;

    // pwm #(
    //     .clock(clock)
    // ) pwm_inst (
    //     .out(pwm),
    //     .clock(clock)
    // );
    // endmodule


module pwm (
    output [7:0] out, 
    input [31:0] clock
);
  reg [7:0] counter;

  always @(posedge clock) begin
    if (counter < 255) begin
      counter <= counter + 1;
    end else begin
      counter <= 0;
    end
  end

  assign out = counter[3:0];
endmodule
